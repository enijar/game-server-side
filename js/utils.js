function applyProps(object, settings) {
    for (let setting in settings) {
        if (settings.hasOwnProperty(setting)) {
            object[setting] = settings[setting];
        }
    }
}
