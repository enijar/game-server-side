'use strict';

(function () {

    // TODO: Fetch this from the database, stored as a JSON string.
    let players = [
        {
            id: 1,
            units: [
                {name: Cleric, tileIndex: 5},
                {name: DarkMagicWitch, tileIndex: 13},
                {name: Enchantress, tileIndex: 19},
                {name: Pyromancer, tileIndex: 14},
                {name: Pyromancer, tileIndex: 18},
                {name: Knight, tileIndex: 26},
                {name: Knight, tileIndex: 27},
                {name: Knight, tileIndex: 28},
                {name: Assassin, tileIndex: 23},
                {name: Scout, tileIndex: 31}
            ]
        },
        {
            id: 2,
            units: [
                {name: Cleric, tileIndex: 120 - 5},
                {name: DarkMagicWitch, tileIndex: 120 - 13},
                {name: Enchantress, tileIndex: 120 - 19},
                {name: Pyromancer, tileIndex: 120 - 14},
                {name: Pyromancer, tileIndex: 120 - 18},
                {name: Knight, tileIndex: 120 - 26},
                {name: Knight, tileIndex: 120 - 27},
                {name: Knight, tileIndex: 120 - 28},
                {name: Assassin, tileIndex: 120 - 23},
                {name: Scout, tileIndex: 120 - 31}
            ]
        }
    ];

    // TODO: Set this as a session.
    const user = {
        id: 1
    };

    const game = new Game({
        node: document.getElementById('arena'),
        width: 600,
        height: 600,
        players: players,
        user: user
    });

    game.renderArena();

}());
