'use strict';

(function () {

    let players = [
        {
            id: 1,
            units: [
                {name: Cleric, tileIndex: 5},
                {name: DarkMagicWitch, tileIndex: 13},
                {name: Enchantress, tileIndex: 19},
                {name: Pyromancer, tileIndex: 14},
                {name: Pyromancer, tileIndex: 18},
                {name: Knight, tileIndex: 26},
                {name: Knight, tileIndex: 27},
                {name: Knight, tileIndex: 28},
                {name: Assassin, tileIndex: 23},
                {name: Scout, tileIndex: 31}
            ]
        },
        {
            id: 2,
            units: [
                {name: Cleric, tileIndex: 120 - 5},
                {name: DarkMagicWitch, tileIndex: 120 - 13},
                {name: Enchantress, tileIndex: 120 - 19},
                {name: Pyromancer, tileIndex: 120 - 14},
                {name: Pyromancer, tileIndex: 120 - 18},
                {name: Knight, tileIndex: 120 - 26},
                {name: Knight, tileIndex: 120 - 27},
                {name: Knight, tileIndex: 120 - 28},
                {name: Assassin, tileIndex: 120 - 23},
                {name: Scout, tileIndex: 120 - 31}
            ]
        }
    ];

    const user = {
        id: 1
    };

    const game = new Game({
        node: document.getElementById('arena'),
        width: 600,
        height: 600,
        players: players,
        user: user
    });

    game.renderArena();

    var unitBases = [
        {name: Knight, title: 'Knight', image: './images/units/knight.gif'},
        {name: Scout, title: 'Scout', image: './images/units/scout.gif'},
        {name: Assassin, title: 'Assassin', image: './images/units/assassin.gif'},
        {name: Cleric, title: 'Cleric', image: './images/units/cleric.gif'},
        {name: Pyromancer, title: 'Pyromancer', image: './images/units/pyromancer.gif'},
        {name: Enchantress, title: 'Enchantress', image: './images/units/enchantress.gif'},
        {name: DragonspeakerMage, title: 'Dragon Speaker Mage', image: './images/units/dragonspeakermage.gif'},
        {name: DarkMagicWitch, title: 'Dragon Magic Witch', image: './images/units/darkmagicwitch.gif'},
        {name: LighteningWard, title: 'Lightning Ward', image: './images/units/lightningward.gif'},
        {name: BarrierWard, title: 'Barrier Ward', image: './images/units/barrierward.gif'},
        {name: BeastRider, title: 'Beast rider', image: './images/units/beastrider.gif'},
        {name: Berserker, title: 'Berserker', image: './images/units/berserker.gif'},
        {name: DragonTyrant, title: 'Dragon Tyrant', image: './images/units/dragontyrant.gif'},
        {name: FrostGolem, title: 'Frost Golem', image: './images/units/frostgolem.gif'},
        {name: Furgon, title: 'Furgon', image: './images/units/furgon.gif'},
        {name: GolemAmbusher, title: 'Golem Ambusher', image: './images/units/golemambusher.gif'},
        {name: GolemAmbusher, title: 'Mud Golem', image: './images/units/mudgolem.gif'},
        {name: PoisonWisp, title: 'Poison Wisp', image: './images/units/poisonwisp.gif'},
        {name: StoneGolem, title: 'Poison Wisp', image: './images/units/stonegolem.gif'}
    ];

    var unitsList = document.querySelector("#units");
    var tiles = document.querySelectorAll(".tile");

    for (let i = 0; i < tiles.length; i++) {
        tiles[i].ondrop = itemDrop;
        tiles[i].ondragover = allowItemDrop;
    }

    // create Unit list
    let i = 0;
    for (let unit of unitBases) {
        let item = document.createElement('div');
        item.ondrag = itemDrag;
        item.title = unit.title;
        item.setAttribute('data-id', i.toString());
        item.setAttribute('draggable', "true");
        item.ondragstart = itemDrag;
        item.className = "item";
        item.width = 75;

        let img = document.createElement('img');
        img.height = 50;
        img.src = unit.image;

        let title = document.createElement('div');
        title.innerHTML = unit.title;

        item.appendChild(img);
        item.appendChild(title);
        unitsList.appendChild(item);

        i++;
    }

    function allowItemDrop(e) {
        e.preventDefault();
    }

    function itemDrop(e) {
        let target = e.target;
        if (target.className.indexOf('tile')) {
            target = e.target.parentNode;
        }

        let data = e.dataTransfer.getData("id");
        let targetIndex = target.className.split('-')[1];

        console.log(data, targetIndex);
        if (targetIndex <= 54) {
            players[0].units.push({name: Cleric, tileIndex: targetIndex});
        }

        if (targetIndex >= 66) {
            players[1].units.push({name: Cleric, tileIndex: targetIndex});
        }

        //game.setUnits(players[0].units);
        //game.setUnits(players[1].units);
        //
        //game.setupPlayers();

        e.preventDefault();
    }

    function itemDrag(e) {
        let id = e.target.parentNode.getAttribute("data-id");
        e.dataTransfer.setData('id', id);
    }

}());
