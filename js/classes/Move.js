class Move {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        applyProps(this, props);

        this.movableTiles = [];
    }

    /**
     * Get index from position.
     *
     * @param {Number} col
     * @param {Number} row
     * @returns {Number}
     */
    getIndexByPosition(col, row) {
        return (row * this.Game.tile.cols) + col;
    }

    /**
     * Get a position from index.
     *
     * @param {Number} index
     * @returns {Object}
     */
    getPositionByIndex(index) {
        return {
            col: index % this.Game.tile.rows,
            row: Math.floor(index / this.Game.tile.rows)
        };
    }

    /**
     * Check the given index does not have a unit in it
     * and the given index is not void.
     *
     * @param {Number} index
     * @returns {Boolean}
     */
    isIndexFree(index) {
        let movable = true;
        let unit = isNaN(this.Game.tile.map[index]) ? this.Game.tile.map[index] : null;

        if (unit !== null) {
            movable = unit.playerId === this.Game.turn.playerId && unit.movable;
        } else {
            movable = this.Game.tile.map[index] > 0;
        }

        return movable;
    }

    /**
     * Check index is the start position.
     *
     * @param index
     * @returns {Boolean}
     */
    isStartPosition(index) {
        return this.tileIndex == index;
    }

    /**
     * Check a position is on the map.
     *
     * @param {Number} col
     * @param {Number} row
     * @returns {Boolean}
     */
    isPositionOutOfBound(col, row) {
        // TODO: Check the corners
        return col < 0 || col >= this.Game.tile.cols || row < 0 || row >= this.Game.tile.rows;
    }

    /**
     * Get movable tile indexes and add them to the movableTiles array.
     *
     * @param {Number|undefined} index (recursive method, it's set itself)
     * @param {Number=0} step
     */
    findPath(index = undefined, step = 0) {
        // Is the first call?
        if (index === undefined) {
            index = this.tileIndex;
            step = 0;
        }

        // Convert index to position
        let position = this.getPositionByIndex(index);
        let col = position.col;
        let row = position.row;

        // Stop the recursive call if one of the following is true:
        // • Current step is greater than the unit's move distance
        // • Unit is not in a free position and that position is not the starting position
        if (step > this.movement || (!this.isIndexFree(index) && !this.isStartPosition(index))) {
            return;
        }

        // If it's not start position the way is free
        if (!this.isStartPosition(index) && this.movableTiles.indexOf(index) < 0) {
            this.movableTiles.push(index);
        }

        // Check left
        if (!this.isPositionOutOfBound(col - 1, row + 0)) {
            this.findPath(this.getIndexByPosition(col - 1, row + 0), step + 1);
        }

        // Check bottom
        if (!this.isPositionOutOfBound(col + 0, row + 1)) {
            this.findPath(this.getIndexByPosition(col + 0, row + 1), step + 1);
        }

        // Check right
        if (!this.isPositionOutOfBound(col + 1, row + 0)) {
            this.findPath(this.getIndexByPosition(col + 1, row + 0), step + 1);
        }

        // Check top
        if (!this.isPositionOutOfBound(col + 0, row - 1)) {
            this.findPath(this.getIndexByPosition(col + 0, row - 1), step + 1);
        }
    }

    getMovableTiles() {
        return this.movableTiles;
    }

}
