class Game {

    /**
     * @constructor
     * @param {Object} settings
     */
    constructor(settings) {
        this.node = settings.node;
        this.width = settings.width;
        this.height = settings.height;
        this.players = settings.players;
        this.user = settings.user;
        this.tile = {
            rows: 11,
            cols: 11,
            map: [
                0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0,
                0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
                0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0
            ]
        };
        this.turn = {
            playerId: 1
        };

        this.setupPlayers();
    }

    /**
     * Sets the player units into the game.
     */
    setupPlayers() {
        let units = [];

        this.players.map((player) => {
            player.units.map((unit) => {
                units.push(
                    new unit.name({
                        Game: this,
                        tileIndex: unit.tileIndex,
                        playerId: player.id
                    })
                );
            });
        });

        this.setUnits(units);
    }

    /**
     * Iterate over all tiles, calling the given callback
     * on each iteration.
     *
     * @param {Function} callback
     */
    tileIteration(callback) {
        let index = 0;

        for (let row = 0; row < this.tile.rows; row++) {
            for (let col = 0; col < this.tile.cols; col++) {
                let tile = this.tile.map[index];
                callback({row, col, index, hasUnit: () => isNaN(tile)});
                index++;
            }
        }
    }

    /**
     * Inserts the given unit instances into the game
     * map array.
     *
     * @param {Array} units
     */
    setUnits(units) {
        this.tileIteration((tile) => {
            units.map((unit) => {
                if (unit.tileIndex === tile.index) {
                    this.tile.map[tile.index] = unit;
                }
            });
        });
    }

    /**
     * Return a tile <div> with the correct className.
     *
     * @param tile
     * @returns {Element}
     */
    getTile(tile) {
        let node = document.createElement('div');
        node.className = `tile index-${tile.index} ${this.tile.map[tile.index] === 0 ? 'void' : ''}`;
        node.innerHTML = '<span>' + (tile.hasUnit() ? this.tile.map[tile.index].name : tile.index) + '</span>';

        if (tile.hasUnit()) {
            node.className += ' unit' + (this.tile.map[tile.index].playerId !== 1 ? ' enemy' : '');

            if (this.tile.map[tile.index].playerId === this.user.id) {
                node.addEventListener('click', () => {
                    let unit = this.tile.map[tile.index];
                    let tiles = unit.getMovableTiles();
                    let tileElements = document.getElementsByClassName('tile');

                    for (let i = 0; i < tileElements.length; i++) {
                        tileElements[i].className = tileElements[i].className.split('movable').join('');
                    }

                    let movableTiles = document.querySelectorAll('.tile.index-' + tiles.join(', .tile.index-'));

                    for (let i = 0; i < movableTiles.length; i++) {
                        movableTiles[i].className += ' movable';
                    }
                }, false);
            }

            node.title = `PlayerID: ${this.tile.map[tile.index].playerId}`;
        }

        return node;
    }

    /**
     * Renders the arena from the current game
     * map array.
     */
    renderArena() {
        this.node.style.width = `${this.width}px`;
        this.node.style.height = `${this.height}px`;

        this.tileIteration((tile) => {
            tile = this.getTile(tile);
            tile.style.width = `${this.width / this.tile.rows}px`;
            tile.style.height = `${this.height / this.tile.cols}px`;
            this.node.appendChild(tile);
        });
    }

}
