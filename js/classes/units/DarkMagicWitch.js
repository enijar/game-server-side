class DarkMagicWitch extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'DarkMagicWitch',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 28,
            power: 24,
            armor: 0,
            blocking: {
                front: 20,
                side: 10,
                back: 0
            },
            recovery: 3,
            movement: 3,
            blockable: false,
            movable: true // Moves out of the way for friendly units
        });
    }

}
