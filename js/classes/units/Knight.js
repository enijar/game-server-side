class Knight extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'Knight',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 50,
            power: 22,
            armor: 25,
            blocking: {
                front: 80,
                side: 40,
                back: 0
            },
            recovery: 1,
            movement: 3,
            blockable: true,
            movable: true // Moves out of the way for friendly units
        });
    }

}
