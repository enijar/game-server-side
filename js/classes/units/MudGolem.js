class MudGolem extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'MudGolem',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 60,
            power: 20,
            armor: 0,
            blocking: {
                front: 0,
                side: 0,
                back: 0
            },
            recovery: 1,
            movement: 4,
            blockable: true,
            movable: true // Moves out of the way for friendly units
        });
    }

}
