class BarrierWard extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'BarrierWard',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 32,
            power: 'barrier',
            armor: 0,
            blocking: {
                front: 100,
                side: 100,
                back: 100
            },
            recovery: 2,
            movement: 0,
            blockable: false,
            movable: false // Moves out of the way for friendly units
        });
    }

}
