class DragonspeakerMage extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'DragonspeakerMage',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 30,
            power: 15,
            armor: 0,
            blocking: {
                front: 33,
                side: 16,
                back: 0
            },
            recovery: 3,
            movement: 3,
            blockable: false,
            movable: true // Moves out of the way for friendly units
        });
    }

}
