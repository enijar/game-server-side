class Cleric extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'Cleric',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 24,
            power: 12,
            armor: 0,
            blocking: {
                front: 0,
                side: 0,
                back: 0
            },
            recovery: 5,
            movement: 3,
            blockable: false,
            movable: true // Moves out of the way for friendly units
        });
    }

}
