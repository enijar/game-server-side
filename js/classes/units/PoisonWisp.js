class PoisonWisp extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'PoisonWisp',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 34,
            power: 'poison',
            armor: 0,
            blocking: {
                front: 0,
                side: 0,
                back: 0
            },
            recovery: 2,
            movement: 6,
            blockable: false,
            movable: true // Moves out of the way for friendly units
        });
    }

}
