class Assassin extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'Assassin',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 35,
            power: 18,
            armor: 12,
            blocking: {
                front: 70,
                side: 35,
                back: 0
            },
            recovery: 1,
            movement: 4,
            blockable: true,
            movable: true // Moves out of the way for friendly units
        });
    }

}
