class LighteningWard extends Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        super({
            name: 'LighteningWard',
            Game: props.Game,
            playerId: props.playerId,
            tileIndex: props.tileIndex,
            hp: 56,
            power: 30,
            armor: 18,
            blocking: {
                front: 100,
                side: 100,
                back: 100
            },
            recovery: 4,
            movement: 0,
            blockable: false,
            movable: false // Moves out of the way for friendly units
        });
    }

}
