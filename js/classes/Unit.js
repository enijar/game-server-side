class Unit {

    /**
     * @constructor
     * @param {Object} props
     */
    constructor(props) {
        applyProps(this, props);

        this.Move = new Move(props);
    }

    /**
     * Returns and array of movable tile indexes
     * for this unit.
     *
     * @returns {Array}
     */
    getMovableTiles() {
        this.Move.findPath();
        return this.Move.movableTiles;
    }

}
